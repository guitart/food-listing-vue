import Vue from 'vue';
import './plugins/vuetify';
import AuthPlugin from './plugins/auth';
import App from './App.vue';
import './registerServiceWorker';
import router from './router';

Vue.use(AuthPlugin);

Vue.config.productionTip = false;

new Vue({
  router,
  render: h => h(App),
}).$mount('#app');
